**ServerTempoServer** is a script that was made to help administrators to save money while the electricity prices are the highest by automatically turning off a server.

The times used in this documentation are Paris time.

# Context
During winter, the elecricity demand is verry high some days. In order to relieve the presure on the electricity netowrk, *Électricité de France* (EDF, French national electricity producer) introduced the *Effacement Jour de Pointe* (EJP) tariff : in exchange of a very low cost during the majority of the year, the electricity is sold at a prohibitive price during the 22 tensest days of the year. The EJP tariff is no longer proposed since 1998, replaced by *Tempo* tariff (that was created in 1995).

The *Tempo* tariff works in a same way, but it is more complex :
- It has off time peaks (from 22:00 to 06:00 the day after)
- Three tarifs are composing it :
    - BLUE, 300 days during all the year, with unbeatable costs, including during peak time
    - WHITE, 43 days during all the year (except sundays), with intresting costs, including during peak time
    - RED, 22 days from the 01/11 to the 31/03 (except weekends and french days off), with prohibitive prices during peak time.

# Script behavior
Once ran, the script will check the *Tempo* color of the day after and display the information on the standard output. In case of a *Tempo* RED day, the script will shedule a poweroff at 06:00.

As the *Tempo* color of the day after is updated everyday at 10:40 on [RTE API](https://data.rte-france.com/catalog/-/api/consumption/Tempo-Like-Supply-Contract/v1.1), it is better to run the script arround 11:00.

# Prerequisites
The user shall create a [RTE API web application](https://data.rte-france.com/group/guest/apps) to get a **Client ID** and **Secret ID**. The user shall export those values in the environnment variables **RTE_APP_ID** and **RTE_APP_SECRET**.

```sh
$ export RTE_APP_ID=12345678-1234-1234-1234-1234567890ab
$ export RTE_APP_SECRET=12345678-1234-1234-1234-1234567890ab
```

# Installation
An idea to install this script on your server is to call it from the crontab.
```sh
0 11 * * * /path/to/your/serverTempoSaver.sh | logger
```

# TODO
- [ ] Make the tempo color extraction more reliable (use a JSON parser)
- [ ] Make error handling more exhaustive
- [ ] Log actions into system journal.
- [ ] help menu (`getopts`)
- [ ] Better installation documentation
- [ ] Email alerting
- [ ] Check D-day tempo for alerting in case the server is being powered on during a TEMPO red peak time.

# References
- [EDF's description of *Tempo* tariff](https://particulier.edf.fr/fr/accueil/gestion-contrat/options/tempo/details.html)
- [EDF's regulated rates price lists (*Tempo* tariff is part of the regulated rates)](https://particulier.edf.fr/content/dam/2-Actifs/Documents/Offres/Grille_prix_Tarif_Bleu.pdf)
- [*Tempo* Like Supply Contract API](https://data.rte-france.com/catalog/-/api/consumption/Tempo-Like-Supply-Contract/v1.1)
- [Interractive *Tempo* calendar](https://www.services-rte.com/fr/visualisez-les-donnees-publiees-par-rte/calendrier-des-offres-de-fourniture-de-type-tempo.html)

# Author
This script was written by Romain GOUPIL.