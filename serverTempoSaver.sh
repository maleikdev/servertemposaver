#!/bin/bash

# Getting the next day time
# Forcing timezone on Paris because tempo tariff applies on mainland France 
TOMORROW_MIDNIGHT=$(TZ=Europe/Paris date --date="tomorrow" +%FT00:00:00%:z)
TWO_DAYS_LATER_MIDNIGHT=$(TZ=Europe/Paris date --date="tomorrow tomorrow" +%FT00:00:00%:z)
TOMORROW_FULL_DATE=$(date --date=tomorrow +%F)

RTE_OAUTH_API=https://digital.iservices.rte-france.com/token/oauth/
RTE_TEMPO_API=https://digital.iservices.rte-france.com/open_api/tempo_like_supply_contract/v1/tempo_like_calendars

# Obtaining OAUTH token
OAUTH_TOKEN=$(curl -s $RTE_OAUTH_API -X POST -u $RTE_APP_ID:$RTE_APP_SECRET | jq -r ."access_token")

if [[ "${OAUTH_TOKEN}" == "" ]]
then
    echo -e "\033[0;31mError : check if your env vars are set." >&2
    exit 0
elif [[ "${OAUTH_TOKEN}" == "null" ]]
then
    echo -e "\033[0;31mError : token is null check your API keys." >&2
    exit 0
fi

# Get tomorrow's Tempo color
TEMPO_RESPONSE=$(curl -s $RTE_TEMPO_API"?start_date="$TOMORROW_MIDNIGHT"&end_date="$TWO_DAYS_LATER_MIDNIGHT --oauth2-bearer $OAUTH_TOKEN -H "Accept: application/json")


if [[ "$TEMPO_RESPONSE" == *"BLUE"* ]]
then
    echo "Tomorrow ("$TOMORROW_FULL_DATE") is a TEMPO BLUE day."
elif [[ "${TEMPO_RESPONSE}" == *"WHITE"* ]]
then
    echo "Tomorrow ("$TOMORROW_FULL_DATE") is a TEMPO WHITE day."
elif [[ "${TEMPO_RESPONSE}" == *"RED"* ]]
then
    echo "Tomorrow ("$TOMORROW_FULL_DATE") is a TEMPO RED day."
    /usr/sbin/shutdown -P 05:55 "Because of Tempo RED day, system will go power-off."
else
    echo -e "\033[0;31mError : no TEMPO color in API response." >&2
fi